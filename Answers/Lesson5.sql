/* 
Exercise 5.1:
Using data in the GRADES table
Use the aggregate functions to create the following columns
Create a column called CtScore which contains the count of Scores in the table
Create a column called CtID which contains the unique count of StudentID in the table
Create a MaxScore column which contains the Maximum Score in the table
Create a MinScore column which contains the Minimum Score in the table
Create a AvgScore column which contains the Average Score in the table
*/

SELECT COUNT(Score) AS CtScore,
COUNT(Distinct StudentID) AS CtID,
MAX(Score) AS MaxScore,
MIN(Score) AS MinScore,
AVG(Score) AS AvgScore
FROM Grades;

/* 
Exercise 5.2:
Using your query from the last exercise cluster the statistics by Course
To cluster your data include the Course column in your SELECT statement
Additionally, use a GROUP BY statement at the end of your query
Lastly, order the results by AvgScore from lowest to highest using the ORDER BY statement
*/
SELECT Course,
COUNT(Score) AS CtScore,
COUNT(Distinct StudentID) AS CtID,
MAX(Score) AS MaxScore,
MIN(Score) AS MinScore,
AVG(Score) AS AvgScore
FROM Grades
GROUP BY Course
Order by AvgScore ASC;

/* 
Exercise 5.3:
Using your query from the last exercise filter the clustered scores
Filter out any clustered AvgScore which is less than the total AvgScore using the HAVING clause
You can obtain the value for the total AvgScore by referring to the output from Exercise 5.1
*/
SELECT Course,
COUNT(Score) AS CtScore,
COUNT(Distinct StudentID) AS CtID,
MAX(Score) AS MaxScore,
MIN(Score) AS MinScore,
AVG(Score) AS AvgScore
FROM Grades
GROUP BY Course
HAVING AvgScore <= 72.813;

/* 
Exercise 5.4
Using data in the GRADES table
Use the aggregate functions to create the following columns
Create a MaxScore column which contains the Maximum Score in the table
Create a MinScore column which contains the Minimum Score in the table
Create a DiffScore column which contains the difference between the Maximum and Minimum Score in the table
Cluster your data by Course
Lastly, order the results by DiffScore from lowest to highest using the ORDER BY statement
*/
SELECT Course,
MAX(Score) AS MaxScore,
MIN(Score) AS MinScore,
MAX(Score) - MIN(Score) AS DiffScore
FROM Grades
GROUP BY Course
Order by DiffScore;

/* 
Exercise 5.5:
Using your query from Exercise 5.2 cluster the statistis by Course and Semester
Filter out any clustered AvgScore which is less than the total AvgScore using the HAVING clause
You can obtain the value for the total AvgScore by referring to the output from Exercise 5.1
*/
SELECT Course,
Semester,
COUNT(Score) AS CtScore,
COUNT(Distinct StudentID) AS CtID,
MAX(Score) AS MaxScore,
MIN(Score) AS MinScore,
AVG(Score) AS AvgScore
FROM Grades
GROUP BY Course, Semester
Order by AvgScore ASC;

/* 
Exercise 5.6:
Using your query from Exercise 5.1 cluster the statistics by Semester and Year
To cluster your data include the Semester and Year column in your SELECT statement
Additionally, use a GROUP BY Semester and Year after the FROM statement in your query
Lastly, order the results by AvgScore from lowest to highest using the ORDER BY statement
*/
SELECT Semester,
Year,
COUNT(Score) AS CtScore,
COUNT(Distinct StudentID) AS CtID,
MAX(Score) AS MaxScore,
MIN(Score) AS MinScor,
AVG(Score) AS AvgScore
FROM Grades
GROUP BY Semester, YEAR
ORDER by AvgScore ASC;
