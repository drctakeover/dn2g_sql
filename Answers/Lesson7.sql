/* 
Exercise 7.1:
Create a table called UNION_FACULTY
Select all of the columns from the FACULTY table
UNION them with all the columns from the NEWFACULTY table
	in such a way that any duplicate rows are removed
*/
CREATE TABLE UNION_FACULTY AS SELECT *
FROM FACULTY
UNION
SELECT *
FROM NEWFACULTY;

/* 
Exercise 7.2:
Create a table called UNIONALL_FACULTY
Select all of the columns from the FACULTY table 
UNION ALL with all the columns from the NEWFACULTY table 
	to combine tables and keep any duplicate rows
*/
CREATE TABLE UNIONALL_FACULTY AS SELECT *
FROM FACULTY
UNION ALL
SELECT *
FROM NEWFACULTY;

/* 
Exercise 7.3:
Create a new table called COURSES
Select the unique values from the Course, Semester, and Year columns
	located in the GRADES table
*/
CREATE TABLE COURSES AS 
SELECT DISTINCT Course,
Semester,
Year
FROM GRADES;

/* 
Exercise 7.4:
Create a new table called LEFT_SIDE
Use the FACULTY (on left) and the COURSES (on right) tables
Create a LEFT JOIN which shows all faculty members (in FACULTY table)
	along with the courses they taught whether they taught or not (in COURSES table) 
Select the FirstName, LastName, Course (from FACULTY table), Semester, and Year from the tables
Use the Course in each table as the join key
The join key condition is when both Course columns are equal
*/
CREATE TABLE LEFT_OUTER AS
SELECT FirstName,
LastName,
A.Course,
Semester,
Year
FROM UNION_FACULTY AS A
LEFT JOIN COURSES AS B
ON A.Course=B.Course;

/* 
Exercise 7.5
Create a new table called RIGHT_SIDE
Use the FACULTY (on left) and the COURSES (on right) tables
Create a RIGHT JOIN which shows all courses (in COURSES table)
	along with the faculty assigned to teach the course taught whether 
	there was a teacher assigned or not (in FACULTY table) 
Select the FirstName, LastName, Course (from COURSES table), Semester, and Year from the tables
Use the Course in each table as the join key
The join key condition is when both Course columns are equal
*/
CREATE TABLE RIGHT_OUTER AS
SELECT FirstName,
LastName,
B.Course,
Semester,
Year
FROM UNION_FACULTY AS A
LEFT JOIN COURSES AS B
ON A.Course=B.Course;

/* 
Exercise 7.6:
Create a new table called FACULTY_COURSES
Select all of the columns from the LEFT_SIDE table
UNION them with all the columns from the RIGHT_SIDE table to ensure 
	any duplicate rows are removed
*/
CREATE TABLE FACULTY_COURSES AS
SELECT * FROM LEFT_OUTER
UNION
SELECT * FROM RIGHT_OUTER;

