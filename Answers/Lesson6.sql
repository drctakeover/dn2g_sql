/* 
Exercise 6.1:
Use the STUDENTS and the CAREER table
The join type you choose should only show students who 
	exist in both the STUDENTS and CAREER table
Select the FirstName, LastName, Track, and Year from the tables
Use the StudentID in each table as the join key
The join key condition is when both StudentID columns are equal
*/
SELECT FirstName,
LastName,
Track,
Year
FROM STUDENTS
INNER JOIN CAREER
ON Students.StudentID = Career.StudentID;

/* 
Exercise 6.2:
Use the STUDENTS and the CAREER table
Create a LEFT JOIN which shows all students who registered (in STUDENTS table)
	along with their career track whether it exists or not (in CAREER table) 
Select the FirstName, LastName, Track, and Year from the tables
Use the StudentID in each table as the join key
The join key condition is when both StudentID columns are equal
Use a WHERE statement to find where the career Track is NULL to see which students
	haven't chosen a career track
*/
SELECT A. StudentID,
FirstName,
LastName,
Track,
Year
FROM STUDENTS AS A
LEFT JOIN CAREER AS B
ON A.StudentID = B.StudentID
WHERE Track IS NULL;

/* 
Exercise 6.3:
Use the CAREER and the STUDENTS tables
Create a LEFT JOIN from the STUDENTS table to the CAREER table
Select the FirstName, LastName, Track, and Year from the tables
Use the StudentID in each table as the join key
The join key condition is when both StudentID columns are equal
Use a WHERE statement to find records where the career Track is NULL to identify students that have not chosen a career track
*/
SELECT B.StudentID,
FirstName,
LastName,
Track,
Year
FROM CAREER AS A
RIGHT JOIN STUDENTS AS B
ON A.StudentID = B.StudentID
WHERE Track IS NULL;

/* 
Exercise 6.4:
Use the STUDENTS and the CAREER tables
Create a LEFT JOIN which shows all students who chose a 
	career track (in CAREER table)	along with their 
	registration information registered whether it exists or 
	not (in STUDENTS table) 
Select the FirstName, LastName, Track, and Year from the tables
Use the StudentID in each table as the join key
The join key condition is when both StudentID columns are equal
Use a WHERE statement to find where the FirstName is
	NULL to see which students haven't registered
*/
SELECT A.StudentID,
FirstName,
LastName,
Track,
Year
FROM CAREER AS A
LEFT JOIN STUDENTS AS B
ON A.StudentID = B.StudentID
WHERE FirstName is NULL;

/*
Exercise 6.5:
Use the STUDENTS and the CAREER tables
Create a RIGHT JOIN which shows all students who chose a
	career track (in CAREER table)	along with their
	registration information registered whether it exists or
	not (in STUDENTS table)
Select the FirstName, LastName, Track, and Year from the tables
Use the StudentID in each table as the join key
The join key condition is when both StudentID columns are equal
Use a WHERE statement to find where the FirstName is
	NULL to see which students haven't registered
*/
SELECT B.StudentID,
FirstName,
LastName,
Track,
Year 
FROM STUDENTS AS A
RIGHT JOIN Career AS B
ON A.StudentID = B.StudentID
WHERE FirstName is NULL;
