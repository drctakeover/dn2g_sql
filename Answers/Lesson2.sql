#Lesson 2.1
CREATE DATABASE TEST_DATABASE; 

SHOW DATABASES; 

#Lesson 2.2
USE TEST_DATABASE;

CREATE TABLE TEST
	(
	RowID VARCHAR(6),
    Name VARCHAR(50),
    BirthDate DATE,
    FavoriteFood VARCHAR(20),
    LuckyNumber TINYINT
    );
    
SHOW TABLES;

#Lesson 2.3
INSERT INTO TEST 
	(
    RowID,
    Name,
    Birthdate,
    FavoriteFood,
    LuckyNumber)
    VALUES
    ("000001","Jade","1995-09-24","Apple Pie",27),
    ("000002","Elliot","2001-02-22","Dried Squid",95);
    
SELECT * FROM TEST;

#Lesson 2.5
LOAD DATA INFILE 'C:/temp/sqldataguru/add_test_data.csv' 
INTO TABLE TEST 
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"'
IGNORE 1 ROWS;

Select * FROM TEST;

#Day 3 Prep
SOURCE C:/temp/dataguru.sql;

SHOW DATABASES;

USE DATAGURU;

SHOW TABLES;

