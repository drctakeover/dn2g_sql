/* 
Exercise 4.1:
Create a new table called HIST_SCORES
The table should include all columns from the Grades table 
Filter to obtain only the rows where "HIST 396" is the course
The semester is "Fall" and the year is 2019
*/
USE DATAGURU;

CREATE TABLE HIST_SCORES AS
SELECT *
FROM GRADES
WHERE Course = "HIST 396"
AND Semester = "Fall"
AND Year = 2019;

/* 
Exercise 4.2:
Create a new table called HIST_STATUS
The table should include all columns from the HIST_SCORES table
Add a new column called Status which shows whether the student
	passed or failed the history course. Use CASE statement 
	with the following conditions to create the new column:
	- If the Score is less than 70, then 'FAIL'
	- If the Score is greater than or equal to 70, then 'PASS'
	- Otherwise leave it blank using two single quotes
*/
CREATE TABLE HIST_STATUS AS
SELECT * , 
(CASE
WHEN Score < 70 THEN "FAIL" 
WHEN Score >= 70 THEN "PASS" 
ELSE ""
END) AS Status 
FROM HIST_SCORES;

/* 
Exercise 4.3:
Create a new table called HIST_BONUS
The table should include all columns from HIST_STATUS table
Add a new column Bonus which shows the current Score with a 35% increase.
Use the equation Score + (Score * .35) to create the new 	column
*/
CREATE TABLE HIST_BONUS AS
SELECT * , 
Score + (Score *.35) AS Bonus
FROM HIST_STATUS;

/*
Exercise 4.4:
Create a new table called HIST_FINAL
The table should include all columns from the HIST_BONUS table
Add a new column called BStatus which shows whether the student
	passed or failed the history course with a Bonus score. 
	Use CASE statements with the following conditions to create 
	the new column:
	- If Bonus is less than 70, then 'FAIL'
	- If the Bonus is greater than or equal to 70, then 'PASS'
	- Otherwise leave it blank using space between two single quotes
*/
CREATE TABLE HIST_FINAL AS
SELECT * , 
(CASE
WHEN Bonus < 70 THEN "FAIL" 
WHEN Bonus >= 70 THEN "PASS" 
ELSE ""
END) AS BStatus 
FROM HIST_BONUS;

/* 
Exercise 4.5:
Run a query which outputs the following to the screen:
Use a string function to create a new column called Subject
Create this column by obtaining only the first
	4 characters in the Course column which is located
	in the Grades table
Only display the first 10 rows to the screen
*/
#Lesson 4.5a
SELECT LEFT(Course, 4) AS Subject
FROM GRADES
LIMIT 10;

#Lesson 4.5b
SELECT SUBSTRING(Course,1, 4) AS Subject
FROM GRADES
LIMIT 10;

/* 
Exercise 4.6:
Create a new table called PROV_REPORT
The table should pull data from the Grades table and will only include the 2 new columns below:
Add the new column called Subject that you created in Exercise 4.5
Add another column called LetterGrade which displays the letter which corresponds with the Score. 
Use a CASE statement  with the following conditions to create a column:
	- If the Score is >= 90 and <=100, then  “A”
	- If the Score is >= 80 and < 90, then  “B”
	- If the Score is >= 70 and < 80, then “C”
	- If the Score is >= 60 and < 70, then  “D”
	- If the Score is >= 50 and < 60, then “E”
	- If the Score is >= 0 and < 50, then  “F”
	- Otherwise leave blank using two double quotes 
*/

-- Lesson 4.6a
CREATE TABLE PROV_REPORT AS
SELECT SUBSTRING(Course,1,4) AS Subject,
(CASE 
WHEN Score >= 90 AND Score <= 100 THEN "A"
WHEN Score >= 80 AND Score < 90 THEN "B"
WHEN Score >= 70 AND Score < 90 THEN "C"
WHEN Score >= 60 AND Score < 70 THEN "D"
WHEN Score >= 50 AND Score < 60 THEN "E"
WHEN Score >= 0 AND Score < 50 THEN "F"
ELSE ""
END) AS LetterGrade
FROM Grades;

--Lesson 4.6b
CREATE TABLE PROV_REPORT AS
SELECT SUBSTRING(Course,1,4) AS Subject,
(CASE 
WHEN Score BETWEEN 90 AND 100 THEN "A"
WHEN Score BETWEEN 80 AND 90 THEN "B"
WHEN Score BETWEEN 70 AND 80 THEN "C"
WHEN Score BETWEEN 60 AND 70 THEN "D"
WHEN Score BETWEEN 50 AND 60 THEN "E"
WHEN Score BETWEEN 0 AND 50 THEN "F"
ELSE ""
END) AS LetterGrade
FROM Grades;




