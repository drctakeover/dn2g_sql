/* 
Exercise 3.1:
Using the STUDENTS table
Query a unique list of states using the State column
*/

/* 
Exercise 3.2:
Use the STUDENTS table
Select each student's FirstName, LastName, Email, and PhoneNumber
Use the WHERE statement to filter and return only columns where 
	State is equal to "Crystalmallow"
*/

/* 
Exercise 3.3:
Use the STUDENTS table
Select each student's FirstName, LastName, Email, and PhoneNumber
Use the WHERE statement to filter and return only columns where 
	State is equal to "Crystalmallow" or
	State is equal to "Icewheat" or
	State is equal to "Newdell" or
	State is equal to "Shadowmoor"
*/

/* 
Exercise 3.4:
Use the STUDENTS table
Select each student's FirstName, LastName, Email, and PhoneNumber
Use the WHERE statement to filter and return only columns where 
	State is in a list containing the states 
	("Crystalmallow", "Icewheat", "Newdell", "Shadowmoor")
*/

/* 
Exercise 3.5:
Use the STUDENTS table
Select each student's FirstName, LastName, Email, and PhoneNumber
Use the WHERE statement to filter and return only columns where 
	State is in a list containing the states 
	("Crystalmallow", "Icewheat", "Newdell", "Shadowmoor")
Sort the table by columns State, FirstName, and LastName
*/

/* 
Exercise 3.6:
Create a new table called FINAID_REPORT using the STUDENTS table
Select each student's FirstName, LastName, Email, and PhoneNumber
Use the WHERE statement to filter and return only columns where 
	State is in a list containing the states 
	("Crystalmallow", "Icewheat", "Newdell", "Shadowmoor")
Sort the table by columns State, FirstName, and LastName
*/

/* 
Exercise 3.7
1) The first step is to tell the computer what data to put into the report. The data is in the FINAID_REPORT tables created in the last lesson. In the MySQL Command Line type SELECT * FROM FINAID_REPORT followed by pressing the Enter key.
2) Next, notifiy the computer of where to place the report by typing INTO OUTFILE  followed by the file location and file name finaid_report.csv enclosed in single quotes. Then press the Enter key.
3) Since the output is a CSV file, indicate that the values in the file should be separated by commas by typing FIELDS TERMINATED BY ‘,' then press the Enter key.
4) Next, specify that all string values in the file need to be enclosed in double quotes by typing ENCLOSED BY '"' and then press Enter.
5) Lastly, the computer needs to know how to separate each row of data. To tell the computer to put each row of data in the CSV file on a separate line, type LINES TERMINATED BY '\n'; to indicate the newline character is where the row ends. Then press Enter. 
Go to the file location on your computer to make sure your file exists. You will be able to view it and share it using Microsoft Excel. To get instructions on opening a CSV file in Excel visit csvexcel.dn2g.com
*/
