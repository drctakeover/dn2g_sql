/* 
Exercise 4.1:
Create a new table called HIST_SCORES
The table should include all columns from the Grades table 
Filter to obtain only the rows where "HIST 396" is the course
The semester is "Fall" and the year is 2019
*/

/* 
Exercise 4.2:
Create a new table called HIST_STATUS
The table should include all columns from the HIST_SCORES table
Add a new column called Status which shows whether the student
	passed or failed the history course. Use CASE statement 
	with the following conditions to create the new column:
	- If the Score is less than 70, then 'FAIL'
	- If the Score is greater than or equal to 70, then 'PASS'
	- Otherwise leave it blank using two single quotes
*/

/* 
Exercise 4.3:
Create a new table called HIST_BONUS
The table should include all columns from the HIST_STATUS table
Add a new column Bonus Status which shows the current score with 
	a 35% increase. Use the equation below to create the new 
	column:
		Score + (Score * .35)
*/

/*
Exercise 4.4:
Create a new table called HIST_FINAL
The table should include all columns from the HIST_BONUS table
Add a new column called BStatus which shows whether the student
	passed or failed the history course with a Bonus score. 
	Use CASE statements with the following conditions to create 
	the new column:
	- If Bonus is less than 70, then 'FAIL'
	- If the Bonus is greater than or equal to 70, then 'PASS'
	- Otherwise leave it blank using space between two single quotes
*/

/* 
Exercise 4.5:
Run a query which outputs the following to the screen:
Use a string function to create a new column called Subject
Create this column by obtaining only the first
	4 characters in the Course column which is located
	in the Grades table
Only display the first 10 rows to the screen
*/

/* 
Exercise 4.6:
Create a new table called PROV_REPORT
The table should pull data from the Grades table and will only
	include the 2 new columns below:
Add the new column called Subject that you created include
	Exercise 4.5
Add another column called LetterGrade which displays the 
	letter which corresponds with the Score. Use CASE statement  
	with the following conditions to create the new column:
	- If the Score is between 90 and 100, then  “A”
	- If the Score is between 80 and 89, then  “B”
	- If the Score is between 70 and 79, then “C”
	- If the Score is between 60 and 69, then  “D”
	- If the Score is between 50 and 59, then “E”
	- If the Score is between 0 and 49, then  “F”
	- Otherwise leave it blank using two double quotes 
*/

