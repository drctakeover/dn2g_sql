/* 
Exercise 6.1:
Use the STUDENTS and the CAREER table
The join type you choose should only show students who 
	exist in both the STUDENTS and CAREER table
Select the FirstName, LastName, Track, and Year from the tables
Use the StudentID in each table as the join keyJoin
The join key condition is when both StudentID columns are equal
*/

/* 
Exercise 6.2:
Use the STUDENTS and the CAREER table
Create a LEFT JOIN which shows all students who registered (in STUDENTS table)
	along with their career track whether it exists or not (in CAREER table) 
Select the FirstName, LastName, Track, and Year from the tables
Use the StudentID in each table as the join key
The join key condition is when both StudentID columns are equal
Use a WHERE statement to find where the career Track is NULL to see which students
	haven't chosen a career track
*/

/* 
Exercise 6.3:
Use the STUDENTS and the CAREER tables
Create a RIGHT JOIN which shows all students who registered (in STUDENTS table)
	along with their career track whether it exists or not (in CAREER table) 
Select the FirstName, LastName, Track, and Year from the tables
Use the StudentID in each table as the join key
The join key condition is when both StudentID columns are equal
Use a WHERE statement to find where the career Track is NULL to see which students
	haven't chosen a career track
*/

/* 
Exercise 6.4:
Use the STUDENTS and the CAREER tables
Create a LEFT JOIN which shows all students who chose a 
	career track (in CAREER table)	along with their 
	registration information registered whether it exists or 
	not (in STUDENTS table) 
Select the FirstName, LastName, Track, and Year from the tables
Use the StudentID in each table as the join key
The join key condition is when both StudentID columns are equal
Use a WHERE statement to find where the FirstName is
	NULL to see which students haven't registered
*/

/*
Exercise 6.5:
Use the STUDENTS and the CAREER tables
Create a RIGHT JOIN which shows all students who chose a
	career track (in CAREER table)	along with their
	registration information registered whether it exists or
	not (in STUDENTS table)
Select the FirstName, LastName, Track, and Year from the tables
Use the StudentID in each table as the join key
The join key condition is when both StudentID columns are equal
Use a WHERE statement to find where the FirstName is
	NULL to see which students haven't registered
*/