/*
Exercise 2.1
*/
CREATE DATABASE TEST_DATABASE;
SHOW DATABASES;

/*
Exercise 2.2
*/
CREATE TABLE TEST
	(
	RowID VARCHAR(6),
	Name VARCHAR(50),
	BirthDate DATE,
	FavoriteFood VARCHAR(20),
           LuckyNumber TINYINT
   );

/*
Exercise 2.3
*/
INSERT INTO TEST 
	(
		RowID,
		Name,
		Birthdate,
		FavoriteFood,
		LuckyNumber
	)
VALUES
("000001","Jade","1995-09-24","Apple Pie",27),
("000002","Elliot","2001-02-22","Dried Squid",95);

/*
Exercise 2.4
1) Now that you have the file location, start by typing the LOAD DATA INFILE command followed by the file location enclosed in single quotes. Then press the Enter key.
2) Next, type INTO TABLE TEST so the computer will know which table to store the data in. Then press the Enter key.
3) Next, indicate  that the values in the file are separated by commas by typing FIELDS TERMINATED BY ‘,' then press the Enter key.
4) Next, specify that all string values in the file are enclosed in double quotes by typing ENCLOSED BY '"' and then press Enter. 
5) The computer needs to know how to determine one row from the next. Each row of data in the CSV file is on a separate line, so type LINES TERMINATED BY '\n' to indicate the newline character is where the row  ends and then press Enter. 
6) The CSV file contains column names in the first row, but the column names are already present in the table, therefore, we will skip the first row so it isn't imported. Type IGNORE 1 ROWS; and press the Enter key.
To see if the rows were added to the table, type SELECT * FROM TEST; and view all of the data currently stored in the TEST table.
*/